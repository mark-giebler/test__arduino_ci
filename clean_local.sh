#!/bin/bash
# 2021-09-16
# Mark Giebler
#
# For running locally to clean up the project tree and remove the left over
# gunk from local build testing.  This gunk can add up to multiple gigabytes of files.

# From Project: https://gitlab.com/mark-giebler/test__arduino_ci

# remove the gunk left by local ./create-build-env.sh run
[[ -f ___F_WORKDIR ]] && rm -Rf $(cat ___F_WORKDIR)
[[ -d ___arduinocli ]] && rm -Rf ___arduinocli
[[ -f arduino-cli.yaml ]] && rm arduino-cli.yaml
rm -f ___F_* 
rm -f ___H_* 

# remove the gunk left by local arduino-cli build
# e.g. arduino-cli compile --fqbn esp32:esp32:heltec_wifi_lora_32_V2 --export-binaries --config-file ./arduino-cli.yaml
rm -Rf build

# remove the gunk left by local gitlab-runner build
# e.g. gitlab-runner exec shell compile
rm -Rf builds

date
