# Test Arduino CI Build in Gitlab And Locally

Author: Mark Giebler

This projects demonstrates automatic configuration of an Arduino build environment using my **CI Meta Tags** in the source code.
This project allows one to run Gitlab CI builds both locally and in the cloud by
using the [**arduino-cli**](https://arduino.github.io/arduino-cli/).

This project also demonstrates how to do automatic release deployments when a tag is pushed.

Releases for this project can be found on the [Releases page](./../../releases).
[![Release](https://www.gitlab.com/mark-giebler/test__arduino_ci/-/badges/release.svg)](https://www.gitlab.com/mark-giebler/test__arduino_ci/-/releases)

## Table of Contents

[[_TOC_]]

## Example Project

This project's **CI Meta Tags** are currently targeting an [**esp32**](https://www.espressif.com/en/products/socs/esp32) MCU.

The CI YAML script is written such that pushing a tag to the Gitlab server will trigger an automated CI build of the project.
[![Pipeline Status](https://gitlab.com/mark-giebler/test__arduino_ci/badges/master/pipeline.svg)](https://gitlab.com/mark-giebler/test__arduino_ci/-/pipelines)

In addition to cloud CI building, the Project's CI can be tested locally (on Linux, or on Windows via WSL)
using either the [**arduino-cli**](#running-arduino-cli-locally)
and/or using a local [**gitlab-runner**](#running-the-gitlab-ciyml-locally).

Using **arduino-cli** locally to build allows testing the following before pushing to the Gitlab server:

- **create-build-env.sh**
- building of the Arduino code

Using a local **gitlab-runner** to build allows testing the following before pushing to the Gitlab server:

- **.gitlab-ci.yml** script
- **create-build-env.sh**
- building of the Arduino code

>Note: For local testing, be sure to install the [**Prerequisites**](#prerequisites) first.

>Note: The project must be committed to a git repository (locally) before running the gitlab-runner locally.


## Cleaning Up After Testing Locally

Running CI tests locally could result in a lot of gunk taking up multiple gigabytes of space on your local disk drive.
This gunk comes mostly from files downloaded to set up the local Arduino build environment.

To free up disk space, run the clean up script:

```
./clean_local.sh
```
The clean up script will remove all the build output files and downloaded files that resulted from testing CI builds locally.


## My CI Meta Tags

The Arduino C++ source code **test__arduino_ci.ino** file includes my **CI Meta Tags** for specifying the target hardware board
and the URL to the Arduino BSP for that hardware. The **CI Meta Tags** are placed in a comment section in the code.
The **CI Meta Tags** are used by this project's CI scripts to automatically extract information from the source code to configure
the CI build environment for the specific target Arduino board.

The CI Meta Tags I defined are:

- **Target_FQBN==**
- **Target_BSP==**
- **Libraries_NATIVE==**
- **Libraries_URL==**

Each **CI Meta Tag** is set equal to a string for direct input as a CLI argument to the arduino-cli.
This allows this CI project's scripts to be used with any type of Arduino board
by appropriate setting of the **CI Meta Tags**.

> IMPORTANT: **Make sure there are no trailing spaces or tabs after the string!!**

Boards such as:

- Adafruit HUZZAH32
- Adafruit Metro M4
- Heltec WiFi LoRa 32 V2
- STM32 Nucleo Boards
- Fuburino32
- Arduino Uno, etc. etc. etc.

See my [**test__arduino_ci.ino**](https://gitlab.com/mark-giebler/test__arduino_ci/-/blob/master/test__arduino_ci.ino#L17)
source code file for examples of how to set them.

The **Target_FQBN==** string is known as the **Fully Qualified Board Name (FQBN)** and has the format of

	<package>:<arch>:<board>


Assuming you have an Arduino IDE with the correct BSP package installed,
you can find the FQBN by running arduino-cli with the following parameters:

```
arduino-cli board search
```

Run the command in a directory without an Arduino arduino-cli.yaml file.

Some board specifiers can be further refined with a forth ```<parameters>``` field.

	<package>:<arch>:<variant>:<parameters>

where ```<parameters>``` is an optional comma-separated list of board specific parameters that are normally shown in the Arduino IDE
in sub-menus of the "Tools" menu.

<br>Some examples of the ```<parameters>``` field in the FQBN I have come across:

- ```<package>:<arch>:<variant>:cpu=<cpu_type>```
- ```<package>:<arch>:<variant>:pnum=<pname>```
- ```<package>:<arch>:<variant>:Version=<ver>```
- ```<package>:<arch>:<variant>:Frequency=<mhz>```
- There are many other forth parameter variants, it is a wild west parameter that any 3rd party BSP creator can define using any keywords of their choosing.
See [here for more info](https://forum.arduino.cc/t/board-parameters-when-compiling-arduino-code-via-command-line/582651/2).

See the **NUCLEO** and **Fuburino** boards in the table further down for examples.
Trying to determine what to use for the fourth parameter can be tricky.
One way is to look in the board.txt file of a BSP package and search for ".build.board=" for possible names.
An easier way is to set verbose mode and build the project with the Arduino IDE and scrutinize
the build log file for the FQBN string used after the command line argument "-fqbn=".

The **Target_BSP==** string is a URL that points to the JSON file for an Arduino Board Support Package (BSP).
The BSP URL ends with ".json",
currently, BSP URLs ending in ".zip" are not supported.

Table of example strings for the CI meta tags. Some boards can have several BSP choices to pick from.

|       Board            |            Target_FQBN==             |  Target_BSP== |
|------------------------|--------------------------------------|---------------|
| Arduino UNO            | arduino:avr:uno                      |   native      |
| [Adafruit HUZZAH32](https://www.adafruit.com/product/3405)    | esp32:esp32:featheresp32:FlashFreq=80,UploadSpeed=921600,DebugLevel=none,PartitionScheme=default | https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_dev_index.json |
| [Metro M4](https://www.adafruit.com/product/3382)  | adafruit:samd:metro_m4   | https://www.adafruit.com/package_adafruit_index.json |
| Heltec WiFi LoRa 32 V2 | esp32:esp32:wifi_lora_32_V2          | https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_dev_index.json |
| Heltec WiFi LoRa 32 V2 | Heltec-esp32:esp32:wifi_lora_32_V2   | https://resource.heltec.cn/download/package_heltec_esp32_index.json |
| [NUCLEO-L031K6](https://www.st.com/en/evaluation-tools/nucleo-l031k6.html)  | STMicroelectronics:stm32:GenL0:pnum=GENERIC_L031K6TX | https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json |
| FubarinoSD (v1.5)     | chipKIT:pic32:fubarino_sd:Version=v15 | https://github.com/chipKIT32/chipKIT-core/raw/master/package_chipkit_index.json |


## Using The CI Meta Tags

The CI meta tag strings are extracted by the CI script files using **grep** and **awk**.

Example to extract the target hardware string from the CI meta data:

```
TARGETHW=$(grep "Target_FQBN==" test__arduino_ci.ino |  awk '{split($0, a, /==/); print a[2]}')
```

Example to extract the target BSP URL string from the CI meta data:

```
TARGETBSPURL=$(grep "Target_BSP==" test__arduino_ci.ino |  awk '{split($0, a, /==/); print a[2]}')
```

>Note: See note below about **arduino-cli's** configuration file command line switch **--config-file** and it's importance.

## Arduino Libraries

Both Arduino Native libraries and 3rd party git cloned libraries can be specified with CI Meta tags
in the source code.

###Native Libraries:

The **Libraries_NATIVE==** string is a comma separated list of native Arduino libraries required for the project.

Example:

```
Libraries_NATIVE==Adafruit SSD1306,Adafruit GFX Library,ArduinoJson
```

###3rd Party Libraries:

The **Libraries_URL==** string is a comma separated list of URLs that points to library git repositories.
The URLs must end with **.git**.

Example:

```
Libraries_URL==https://github.com/me-no-dev/AsyncTCP.git,https://github.com/me-no-dev/ESPAsyncWebServer.git
```


# Local Testing

Local testing refers to testing that the CI scripts function correctly on
the developer's PC, instead of having to test on the Gitlab server.
This can be helpful in CI script development when offline from the internet.
Or if you want to make sure your CI works before pushing it to Gitlab.

## Prerequisites

The local system needs to have some applications installed before local testing of the CI scripts can be performed.

### Install arduino-cli Locally

For testing **arduino-cli** compile scripts locally on a PC running a Linux OS; Install **arduino-cli** to a non-root location
that is in your PATH.

E.g.: ~/.local/bin

Install command:

```
curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=~/.local/bin sh
```

This project was tested using various versions between 0.19.2 and 0.32.2.

More info on installing [here.](https://arduino.github.io/arduino-cli/0.32/installation/)

>Note: The arduino-cli folks tend to make breaking changes on occasion, so if this doesn't work for you,
>try an older version of arduino-cli.


### Install gitlab-runner Locally

**IMPORTANT:** Gitlab will REMOVE the 'exec' option from [gitlab-runner in 2024,](https://docs.gitlab.com/ee/update/deprecations.html#the-gitlab-runner-exec-command-is-deprecated)
thus this section will no longer be relevant for gitlab-runner versions after that.
In that case, use an older version such as [16.4.1](https://gitlab.com/gitlab-org/gitlab-runner/-/releases/v16.4.1).

For testing CI scripts locally on a PC running a Linux OS;
Install the official [**gitlab-runner**](https://gitlab.com/gitlab-org/gitlab-runner) binary.
The commands below will download and install from the official Gitlab download server.

```
OS=linux
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-${OS}-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
```

Alternatively, one can install from an official Gitlab deb package, but this will create and start a daemon.

[https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner](https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner)

List of official binary images:

[https://gitlab-runner-downloads.s3.amazonaws.com/latest/index.html](https://gitlab-runner-downloads.s3.amazonaws.com/latest/index.html)


This project was last tested with gitlab-runner version: [14.2.0](https://gitlab.com/gitlab-org/gitlab-runner/-/releases/v14.2.0)


### Optional: Install Docker Locally

Optional: Install a Linux container engine in order to allow the **gitlab-runner** to run CI scripts
that use Docker containers.

```
sudo apt install docker.io
# check the daemon is running
sudo systemctl status docker
```

> Todo: Add instructions on setting up a gitlab-runner container and sharing the local project directory with it.

## Running The .gitlab-ci.yml Locally


**IMPORTANT:** Gitlab will REMOVE the 'exec' option from [gitlab-runner in 2024,](https://docs.gitlab.com/ee/update/deprecations.html#the-gitlab-runner-exec-command-is-deprecated)
thus this section will no longer be relevant after that.

Run the **gitlab-runner** locally to execute (test) the task **compile** in the **.gitlab-ci.yml** script.

First cd to the project directory containing the **.gitlab-ci.yml** file.

Second execute the **compile** task:

```
gitlab-runner exec shell compile
```

>Note: Be sure to install the [**Prerequisites**](#prerequisites) first.

>Note: The project must be committed to a git repository (locally) before running the gitlab-runner.


## Running arduino-cli Locally

First run  **create-build-env.sh** locally which will download
and configure the arduino build toolchain for the target Arduino board:

```
./create-build-env.sh
```

The "long way" example to run **arduino-cli** locally:

> Note that the --fqbn parameter will depend on your actual target configuration.

```
arduino-cli compile --fqbn esp32:esp32:heltec_wifi_lora_32_V2 --export-binaries --config-file ./arduino-cli.yaml
```

The "short way" example to run **arduino-cli** locally:

```
./___H_a-build.sh
```

The script **___H_a-build.sh** is created by **create-build-env.sh**
with all the correct parameters for your target configuration.


## Uploading to The Target

The locally built binary can be uploaded to the target device on the local PC
using the **___H_upload.sh** script.
The **___H_upload.sh** script is created by **create-build-env.sh**
with all the correct parameters for your target configuration.

**On Linux:**

If the target device is on /dev/ttyUSB0, then simply enter:

```
./___H_upload.sh
```

If the target device is on some other part, specify the port as an argument to the script.

```
./___H_upload.sh /dev/ttyACM1
```


# My Notes

These are notes to myself. You don't need to concern yourself with these things here.

## arduino-cli Directories

To view the configuration that **arduino-cli** will use:

```
arduino-cli config dump
```

Before running a local CI test using **arduino-cli**, it is best to change the configured
directories so as not to clobber your Arduino IDE's configurations.

For example, these default arduino-cli configs will clobber the IDE's config:

```
directories:
  data: /home/mgiebler/.arduino15
  downloads: /home/mgiebler/.arduino15/staging
  user: /home/mgiebler/Arduino
```

[No longer available](https://arduino.github.io/arduino-cli/0.32/UPGRADING/#0320)
with arduino-cli v0.32.0: ~~This can be overridden by placing a configuration file in the same directory as the project.~~

~~Or the Best~~ The **_Only_** approach is to specify the configuration file on the command line with:

```
--config-file ./arduino-cli.yaml
```

This CI project's **.gitlab-ci.yml** uses this approach.


# TODO

1. Add example of using a local gitlab-runner docker container to test CI using containers


# References

You can find the **CI Meta Tags** project [here.](https://gitlab.com/mark-giebler/test__arduino_ci/)

References that were handy in making this all possible:

**Arduino-cli:**

	https://arduino.github.io/arduino-cli/0.27/installation/#use-the-install-script
	https://arduino.github.io/arduino-cli/0.27/configuration/
	https://arduino.github.io/arduino-cli/0.27/commands/arduino-cli_compile/

**Use a docker ubuntu container:**

	https://codeblog.dotsandbrackets.com/gitlab-ci-esp32-arduino/

**Gitlab CI for Arduino project (build on gitlab server):**

	https://codeblog.dotsandbrackets.com/gitlab-ci-esp32-arduino/
	https://gitlab.com/drstevenhale/arduino-ci-example/-/tree/master

**Local CI pipeline testing:**

	https://stackoverflow.com/questions/49090675/how-can-i-test-gitlab-ci-yml
	https://stackoverflow.com/a/52724374
	https://github.com/firecow/gitlab-ci-local
