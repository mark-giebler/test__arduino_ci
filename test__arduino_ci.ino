// -*- mode: C++ -*-
// Mark Giebler
// 2021-09-14
//
// Testing automatic Arduino device selection in Gitlab CI pipeline and on a local PC
// using my CI Meta Tags in the comment section.
// See project README.md for how to use the CI meta data tags.
//
// IDE: Arduino 1.8.16
// IDE Board setting:
// 	    	 ESP32 Arduino --> "Heltec Wifi Lora 32" LoRa + OLED Board
// or		 ESP32 Arduino --> "Heltec Wifi Lora 32 (V2)" Marked on bottom of board: WiFi_LoRa_32_V2
//
//
// ----------BEGIN CI Meta Tag Data----------
//
// Info on my CI Meta Tags: https://gitlab.com/mark-giebler/test__arduino_ci
//
// Declare the target hardware for auto target selection CI code:
// Target_FQBN==esp32:esp32:heltec_wifi_lora_32_V2
//
// BSP download URL for auto target selection CI code:
// Target_BSP==https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
//
// List any Arduino Native libraries required by the sketch:
// Libraries_NATIVE==Adafruit BME280 Library,Adafruit Unified Sensor
//
// List any 3rd-Party Libraries to install from git repos:
// Libraries_URL==https://gitlab.com/mark-giebler/Line_Menu.git,https://github.com/ThingPulse/esp8266-oled-ssd1306.git
//
// ----------END CI Meta Tag Data----------
//
//
// Based on an example of building esp32 arduino project with Gitlab CI and arduino-cli
// from here: https://codeblog.dotsandbrackets.com/gitlab-ci-esp32-arduino/


// -------------------------------------
//
//    The ubiquitous Blinky example
//
// -------------------------------------


#undef LED_BUILTIN
#define LED_BUILTIN	(25)	// HELTEC ESP32 WiFi LoRa 32(V2)

void setup()
{
	// initialize digital pin LED_BUILTIN as an output.
	pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
	digitalWrite(LED_BUILTIN, HIGH);   // LED on
	delay(333);
	digitalWrite(LED_BUILTIN, LOW);    // LED off
	delay(333);
}

// -------------------------------------
