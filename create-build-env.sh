#!/bin/bash

# This will setup the CI build environment based on my CI Meta Tags embedded in the source code file.
# We have various conditional tests such that this can also be run locally outside CI for testing as non-root user.
# This script creates a config file in the project directory named arduino-cli.yaml

# This script takes one option argument that specifies
# the working sub directory to create for the Arduino build environment.

# From Project: https://gitlab.com/mark-giebler/test__arduino_ci

# Mark Giebler  2021-09-14

# 2023-10-18: arduin-cli v0.35 is broken and fails to install.
# Pin version to known working version:
ARDO_CLI_VER=0.34.2

pwd
echo "=================================================="
cat /etc/os-release
echo "=================================================="
apt-get update
echo "=================================================="
echo "Install git if needed"
which git
if [[ $? -ne 0 ]]; then
	apt-get install git -y
else
	echo "SKIP install git - already installed"
fi

# When testing locally, so as not to clobber any existing Arduino IDE install,
# make the directory for storing arduino BSPs, toolchains, and libraries
WORKDIR=${1:-___arduinocli}
echo "WorkDir is: ${WORKDIR}"
echo -e -n ${WORKDIR}/ > ___F_WORKDIR

mkdir -p ${WORKDIR}

echo "=================================================="
echo "Make custom arduino-cli.yaml"
# make a custom arduino-cli config file to override the default config directories
echo "Making  arduino-cli.yaml  using WorkDir  ${WORKDIR}"
cat > ./arduino-cli.yaml << MGEND
board_manager:
  additional_urls: []
daemon:
  port: "50051"
directories:
  data: ./${WORKDIR}/.arduino15
  downloads: ./${WORKDIR}/.arduino15/staging
  user: ./${WORKDIR}/
library:
  enable_unsafe_install: false
logging:
  file: "./${WORKDIR}/log__arduino-cli.txt"
  format: text
  level: debug
metrics:
  addr: :9090
  enabled: true
output:
  no_color: false
sketch:
  always_export_binaries: true
updater:
  enable_notification: false
MGEND

echo "=================================================="
echo "Install arduino-cli if needed"
## https://arduino.github.io/arduino-cli/0.35/installation/
which arduino-cli
if [[ $? -ne 0 ]]; then
	apt-get install curl -y
	echo "=================================================="
	echo "Fetching arduino-cli ${ARDO_CLI_VER} for installation ..."
	curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=/usr/bin sh -s ${ARDO_CLI_VER}
	if [[ $? -ne 0 ]]; then
		echo "arduino-cli download / install FAILED! *************************"
		exit 1
	fi
else
	echo "SKIP install arduino-cli - already installed"
fi
arduino-cli version

echo "=================================================="
echo "Install python 3.x if needed"
which python3
if [[ $? -ne 0 ]]; then
	apt-get install python3 -y
	ln -s /usr/bin/python3 /usr/bin/python
	## Ubuntu 20.04 only
	#apt-get install python-is-python3 -y
else
	echo "SKIP install python3 - already installed"
fi
python3 --version

echo "=================================================="
echo "Install python pip3 and pyserial if needed"
which pip3
if [[ $? -ne 0 ]]; then
##  need pyserial for ESP32 tools.
	apt-get install python3-pip -y
	pip3 install pyserial
else
	echo "SKIP install pip3 - already installed"
fi

echo "=================================================="
echo "Get CI meta data from .ino file to determine required BSP"
## We assume only one .ino file since real programmers will just have the one .ino file and the
## rest of their modular code in normal .c and .cpp files.
export TARGETINO=$(ls *.ino)
if [ -n "${TARGETINO}" ]; then
	# obtain the target board HW (FQBN) and BSP URL from CI meta data in .ino source code
	# Input example:
	#   Target_FQBN==esp32:esp32:featheresp32:FlashFreq=80,UploadSpeed=921600,DebugLevel=none,PartitionScheme=default
	export TARGETHW_FQBN=$(grep "Target_FQBN==" ${TARGETINO} | awk '{split($0, a, /==/); print a[2]}')
	echo "Target Board is [  ${TARGETHW_FQBN}  ]"
	# get first two tokens of the FQBN.  PACKAGER:ARCH
	export TARGETBSP_HW=$(echo ${TARGETHW_FQBN} | awk '{split($0, b, /:/); print b[1] ":" b[2]}')
	# get the URL to the BSP
	# Only supports installing via JSON file for now.
	# Input example:
	#   Target_BSP==https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_dev_index.json
	export TARGETBSP_URL=$(grep "Target_BSP==" ${TARGETINO} | awk '{split($0, a, /==/); print a[2]}')
	echo "Target BSP URL is [  ${TARGETBSP_URL}  ]"
	# get comma separated list of native libraries required
	# Input example:
	#   Libraries_NATIVE==Adafruit SSD1306,Adafruit GFX Library,ArduinoJson
	export TARGETLIBS_NATIVE=$(grep "Libraries_NATIVE==" ${TARGETINO} | awk '{split($0, a, /==/); print a[2]}')
	echo "Native LIBs are [  ${TARGETLIBS_NATIVE}    ]"
	# get comma separated list of external library URLs required
	# Input example:
	#   Libraries_URL==https://github.com/me-no-dev/AsyncTCP.git,https://github.com/me-no-dev/ESPAsyncWebServer.git
	export TARGETLIBS_URL=$(grep "Libraries_URL==" ${TARGETINO} | awk '{split($0, a, /==/); print a[2]}')
	echo "URL LIBs are [  ${TARGETLIBS_URL}    ]"
fi


# Install the target MCU core
echo "=================================================="
echo "Install Arduino BSP for: ${TARGETBSP_HW}"
# check if JSON
if [[ ${TARGETBSP_URL} == *".json" ]]; then
	grep ${TARGETBSP_URL} ./arduino-cli.yaml
	if [[ $? -ne 0 ]]; then
		echo "Adding URL to config file: [  ${TARGETBSP_URL}  ]"
		sed -i "/additional_urls/c\  additional_urls:\n  - ${TARGETBSP_URL}" ./arduino-cli.yaml
	fi
elif [[ ${TARGETBSP_URL} == "native" || ${TARGETBSP_URL} == "" ]]; then
	echo "Using Native BSP for [  ${TARGETBSP_HW}  ]"
else
	echo "Unsupported BSP URL type (non-json)"
	exit -1
fi

arduino-cli core update-index --config-file ./arduino-cli.yaml
arduino-cli core install ${TARGETBSP_HW} --config-file ./arduino-cli.yaml
if [[ $? -ne 0 ]]; then
	echo "== Failure during BSP install =="
	exit -1
fi

# Install 'native' libraries
echo "=================================================="
echo "Install Native Arduino Libraries from CI Meta tag"
if [[ ${TARGETLIBS_NATIVE} == "" ]]; then
	echo "No Native Libraries specified"
else
	origIFS="$IFS"
	# override default splitting to not include whitespace.
	IFS=","
	for lib in ${TARGETLIBS_NATIVE}
	do
		echo "Install: ${lib}"
		arduino-cli lib install "${lib}"  --config-file ./arduino-cli.yaml
		if [[ $? -ne 0 ]]; then
			echo "Failure during Native Library install"
			IFS="$origIFS"
			exit -1
		fi
	done
	IFS="$origIFS"
fi
echo "Install any Native libs from script"
#arduino-cli lib install "ArduinoJson"  --config-file ./arduino-cli.yaml
#arduino-cli lib install "MPU9250_asukiaaa"  --config-file ./arduino-cli.yaml

# Install 'third-party' libraries: find proper location and 'git clone' them
echo "=================================================="
echo "Install 3rd-party Libraries via URL from CI Meta tag"
if [[ ${TARGETLIBS_URL} == "" ]]; then
	echo "No 3rd party Libraries specified"
else
	retDir=$(pwd)
	# make sure there is a /libraries sub directory
	mkdir -p `arduino-cli config dump --config-file ./arduino-cli.yaml | grep "user:" | sed 's/.*\ //'`/libraries
	cd `arduino-cli config dump --config-file ./arduino-cli.yaml | grep "user:" | sed 's/.*\ //'`/libraries
	pwd

	IFS=","
	for lib in ${TARGETLIBS_URL}
	do
		echo "Install: ${lib}"
		if [[ ${lib} == *".git" ]]; then
			# Workaround if 3rd party libs already exist, for now remove them all and re-clone
			# Todo: if they exist, do a git pull in each directory?
			repoDir=$(echo ${lib} | awk -F / '{print $NF}')
			repoDir=$(echo ${repoDir} | awk -F . '{print $1}')
			echo "repo dir: ${repoDir}"
			rm -Rf ${repoDir}
			git clone ${lib}
			if [[ $? -ne 0 ]]; then
				echo "Failure during git clone install of URL Library"
				IFS="$origIFS"
				exit -1
			fi
		else
			echo "Unsupported Library URL type (non-git)"
			IFS="$origIFS"
			exit -1
		fi
	done
	IFS="$origIFS"

	echo "==== Library install complete. ===="
	# back to the dir we started in.
	cd ${retDir}
	pwd
fi

echo "=================================================="
## rant: Since Arduino CLI v0.10 We need to workaround the most frigging, stupid implementation of arduiono-cli ever. (TM)
## It now ignores the target source file name on the command line,
## instead it assumes the source file name to be the same as the containing directory.
## In the CI build environment, that directory name can change each build.
## Our workaround is to rename the .ino file to match the containing directory name.
## We assume only one .ino file since real programmers will just have the one .ino file
## with the rest of their modular code in normal .c and .cpp named files.
newinoName=${PWD##*/}
echo "Project Build .ino name will be: ${newinoName}.ino"
mv ${TARGETINO} ${newinoName}.ino

echo "creating files that will contain build variables, for use in CI YAML script."
# Filenames used here must be manually coordinated with .gitlab-ci.yml
echo -e -n ${newinoName}.ino > ___F_BUILDINONAME
echo -e -n ${TARGETHW_FQBN} > ___F_TARGETHW_FQBN
echo -e -n ${TARGETBSP_URL} > ___F_TARGETBSP_URL
echo -e -n ${TARGETLIBS_NATIVE} > ___F_TARGETLIBS_NATIVE
echo -e -n ${TARGETLIBS_URL} > ___F_TARGETLIBS_URL

# make various helper scripts for local PC use.
# Use file names with the prefix the clean up script will remove.
# make arduino-ci build helper
cat > ./___H_a-build.sh << MGEND
#!/bin/bash
# Edits to this file  WILL BE LOST.  This file is created automatically.
#
# Build project using
the_cmd="arduino-cli compile --fqbn $(cat ./___F_TARGETHW_FQBN) --config-file ./arduino-cli.yaml"
# Show the version for diagnostic review purposes
arduino-cli version
echo \${the_cmd}
eval \${the_cmd}
date
MGEND
# make gitlab-runner build helper
cat > ./___H_g-build.sh << MGEND
#!/bin/bash
# Edits to this file  WILL BE LOST.  This file is created automatically.
#
# Build project using
the_cmd="gitlab-runner exec shell compile"
# Show the version for diagnostic review purposes
gitlab-runner --version
echo \${the_cmd}
eval \${the_cmd}
date
MGEND
# make upload to target helper
cat > ./___H_upload.sh << MGEND
#!/bin/bash
# Edits to this file  WILL BE LOST.  This file is created automatically.
#
# Upload binary to target
# First argument is communication port to use.
# If no first argument, default to a Linux ttyUSB0 port.
port=\${1:-/dev/ttyUSB0}
the_cmd="arduino-cli upload --fqbn $(cat ./___F_TARGETHW_FQBN) --config-file ./arduino-cli.yaml --port \${port}"
# Show the version for diagnostic review purposes
arduino-cli version
echo \${the_cmd}
eval \${the_cmd}
date
MGEND
chmod +x ___H_*

eval ls -l ___{F,H}_*

echo -e "\n==================================================\n"
echo -e "Build Environment Created\n"
# ready for gitlab CI pipeline to continue with build. Or local builds.
#
# Echo Example build commands:
echo "== Example arduino-cli usage for local test build:"
echo "  arduino-cli compile --fqbn $(cat ./___F_TARGETHW_FQBN) --config-file ./arduino-cli.yaml"
echo -e "  or:\n  ./___H_a-build.sh"
echo "== Example gitlab-runner usage for local test build:"
echo "  gitlab-runner exec shell compile"
echo -e "  or:\n  ./___H_g-build.sh"
echo "== Example arduino-cli upload build to target device:"
echo "  arduino-cli upload --fqbn $(cat ./___F_TARGETHW_FQBN) --config-file ./arduino-cli.yaml --port /dev/ttyUSB0"
echo -e "  or:\n  ./___H_upload.sh /dev/ttyUSB0"

##
echo -e "\n=================================================="
echo -e "$(pwd)\n$0\n\tCompleted build environment creation.\n"
echo -e "==================================================\n"
date
